package com.prop.rent.pages;

import com.prop.rent.utils.Page;

public class FilterPage extends Page{
	
	public FilterPage() {
		super();
	}
	
	final static String PAGE_TITLE = "Singapore Property, Property for Sale/Rent, Singapore Real Estate | PropertyGuru";
	final static String RENT_LINK = "a[href='#rent']";
	final static String SEARCH_BUTTON = "button[class='btn btn-primary btn-submit'] i[class='pgicon pgicon-search']";
	final static String SEARCH_INPUT = "input[placeholder='Type or Select Location']";
	
	public static boolean checkRentPageTitle() {
		if(getTitle().equals(PAGE_TITLE))
			return true;
		else
			return false;
	}
	
	public static boolean filterRentProperties() {
		Page.click(RENT_LINK);
		Page.enterText(SEARCH_INPUT, "Jurong East");
		Page.click(SEARCH_BUTTON);
		return true;
	}
	

}
