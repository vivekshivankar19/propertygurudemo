package com.prop.rent.pages;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.prop.rent.utils.Page;

public class RentPage extends Page{

	public RentPage() {
		super();
	}
	
	// Constant value
	final static int MIN_RENT = 1000;
	final static int MAX_RENT = 2000;
	
	final static String PAGE_TITLE = "Property For Rent, jurong east in Singapore | PropertyGuru Singapore";
	final static String SEARCH_PAGE_TITLE = "Property For Rent, Between S$ 1 K and S$ 2 K in Singapore | PropertyGuru Singapore";
	final static String SEARCH_INPUT = "input[placeholder='Type or Select Location']";
	final static String RENT_LINK = "a[href='#rent']";
	final static String SEARCH_LOCATION_INPUT = "input[name='freetext']";
	final static String PRICE_FILTER_BUTTON = "button[aria-expanded='false'] span[class='btn-title']";
	final static String MIN_PRICE_INPUT = "input[class='form-control focus'][name='minprice']";
	final static String MAX_PRICE_INPUT = "input[class='form-control focus'][name='maxprice']"; // "input[name="maxprice"][xpath="1"]";
	final static String FILTER_SEARCH_BUTTON = "button[class='btn btn-primary btn-submit']";
	final static String TOTAL_RESULT_H1 = "h1[class='title search-title hidden-xs text-transform-none']";
	final static String PRICE_LISTING_TR = "div[class='listing-widget-new small-listing-card '] div[class*='listing-card listing-id']";
	final static String PRICE_LISTING_TD = "div div[class*='listing-description'] ul[class='listing-features'] li:nth-child(1)";
	
	public static boolean checkRentPageTitle() {
		if(getTitle().equals(PAGE_TITLE))
			return true;
		else
			return false;
	}
	
	public static boolean filterRent() {
		Page.verifyElementOnPage(FILTER_SEARCH_BUTTON);
		Page.click(PRICE_FILTER_BUTTON);
		Page.click(MIN_PRICE_INPUT);
		Page.enterText(MIN_PRICE_INPUT, "1000");
		Page.enterText(MAX_PRICE_INPUT, "2000");
		Page.click(FILTER_SEARCH_BUTTON);
		return true;
	}
	
	public static boolean verifySearchDetails() {
		Page.verifyElementOnPage(TOTAL_RESULT_H1);
		List<WebElement> eleList = Page.getElements(PRICE_LISTING_TR);
		
		int  i = 0;
		while(i++ <= eleList.size()) {
			WebElement element = Page.getElement(TOTAL_RESULT_H1+":nth-child("+i+PRICE_LISTING_TD);
			String value = element.getText().replaceAll("[^0-9]", "");
			if(!(Integer.parseInt(value) >= MIN_RENT && Integer.parseInt(value) <= MAX_RENT)) {
				return false;
			}
		}
		
		return true;
	}
	
	
}
