package com.prop.rent.utils;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.prop.base.TestBase;

public class Page extends TestBase{
	
	public Page() {
		super();
	}
	
	public static boolean verifyElementOnPage(String element) {
		if(driver.findElement(By.cssSelector(element)).isDisplayed()
				|| driver.findElement(By.cssSelector(element)).isEnabled()) {
			return true;
		}else {
			return false;
		}
	}
	
	public static List<WebElement> getElements(String element) {
		return driver.findElements(By.cssSelector(element));
	}
	
	public static WebElement getElement(String element) {
		return driver.findElement(By.cssSelector(element));
	}
	
	public static void switchToFrameUsingName(String frameName) {
		driver.switchTo().frame(frameName);
	}
	
	public static void switchToFrameUsingID(int num) {
		driver.switchTo().frame(num);
	}
	
	public static void switchToFrameUsingElement(WebElement frameElement) {
		driver.switchTo().frame(frameElement);
	}
	
	public static void enterText(String element, String value) {
		driver.findElement(By.cssSelector(element)).clear();
		driver.findElement(By.cssSelector(element)).sendKeys(value);
	}
	
	public static void click(String element) {
		driver.findElement(By.cssSelector(element)).isDisplayed();
		driver.findElement(By.cssSelector(element)).click();
	}
	
	public static String getText(String element) {
		return driver.findElement(By.cssSelector(element)).getText();
	}
	
	public static String getTitle() {
		return driver.getTitle();
	}

}
