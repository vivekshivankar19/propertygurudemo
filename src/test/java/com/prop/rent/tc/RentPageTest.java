package com.prop.rent.tc;

import org.junit.AfterClass;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.prop.rent.pages.FilterPage;
import com.prop.rent.pages.RentPage;

public class RentPageTest extends RentPage{
	
	public RentPageTest() {
		super();
	}
	
	@BeforeClass
	public void setUp(){
		init();
	}
	
	@Test(priority=1)
	public void loginPageTitleTest(){
		FilterPage.filterRentProperties();
		Assert.assertTrue(checkRentPageTitle(), "Rent page title is incorrect or not matched");
		Assert.assertTrue(filterRent(), "Filter rent based on price is failed.");
		Assert.assertTrue(verifySearchDetails(), "Result not matched with the passed details.");
	}
	
	@AfterClass
	public void crmLogoImageTest(){
		tearDown();
	}

}
