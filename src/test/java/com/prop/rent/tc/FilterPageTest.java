package com.prop.rent.tc;

import org.junit.AfterClass;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.prop.rent.pages.FilterPage;

public class FilterPageTest extends FilterPage{

	public FilterPageTest() {
		super();
	}
	
	@BeforeClass
	public void setUp(){
		init();
	}
	
	@Test(priority=1)
	public void searchPageTitleTest(){
		Assert.assertTrue(checkRentPageTitle(), "Search page title is not matched");
	}
	
	@Test(priority=1)
	public void searchRentPageFilterTest(){
		Assert.assertTrue(filterRentProperties(), "Search property operation is failed.");
	}
	
	@AfterClass
	public void crmLogoImageTest(){
		tearDown();
	}
	
}
